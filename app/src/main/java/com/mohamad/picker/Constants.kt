package com.mohamad.picker


class Constants {
    companion object {
        //RGB VALUE OF COLORS IN THREE WAY SEGMENTATION
        const val FIRST_COLOR = -10551363
        const val SECOND_COLOR = -8454309
        const val THIRD_COLOR =  -39936
    }
}