package com.mohamad.picker


import android.os.Build
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import com.mohamad.colorwheel.ColorWheel


class MainActivity : AppCompatActivity() {
    private lateinit var colorWheel: ColorWheel
    private lateinit var colorBox1: View
    private lateinit var colorBox2: View
    private lateinit var colorBox3: View
    private lateinit var dot1: View
    private lateinit var dot2: View
    private lateinit var dot3: View
    private lateinit var mSeekBar: SeekBar
    lateinit var viewBackgorund: View


    @RequiresApi(Build.VERSION_CODES.O) //for change RGB
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initView()
        //change Brightness of background
        mSeekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                viewBackgorund.alpha = (p0?.progress!! / 100.0).toFloat()
                colorWheel.alpha = (p0?.progress!! / 100.0).toFloat()
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {
            }

            override fun onStopTrackingTouch(p0: SeekBar?) {
            }

        })


        //arg values are came from android studio
        //move position of thumble circle to color position in wheel color picker
        colorBox1.setOnClickListener {
            setDefaultBackgroundForBoxes()
            colorBox1.setBackgroundResource(R.color.icon_selected)
            colorWheel.rgb = Constants.FIRST_COLOR
        }
        colorBox2.setOnClickListener {
            setDefaultBackgroundForBoxes()
            colorBox2.setBackgroundResource(R.color.icon_selected)
            colorWheel.rgb = Constants.SECOND_COLOR
        }
        colorBox3.setOnClickListener {
            setDefaultBackgroundForBoxes()
            colorBox3.setBackgroundResource(R.color.icon_selected)
            colorWheel.rgb = Constants.THIRD_COLOR
        }


        colorWheel.colorChangeListener = { rgb: Int ->
            viewBackgorund.setBackgroundColor(rgb)

        }

    }

    private fun initView() {

        //todo codes bellow have been deprecated add DataBinding library and refactor the code
        colorWheel = findViewById(R.id.colorWheel)
        mSeekBar = findViewById(R.id.thumbRadiusSeekBar)
        dot1 = findViewById(R.id.dot_1)
        dot2 = findViewById(R.id.dot_2)
        dot3 = findViewById(R.id.dot_3)
        colorBox1 = findViewById(R.id.color_box_1)
        colorBox2 = findViewById(R.id.color_box_2)
        colorBox3 = findViewById(R.id.color_box_3)
        viewBackgorund = findViewById(R.id.view_backgorund)
    }

    //set all backgroud of each box to on selected
    private fun setDefaultBackgroundForBoxes() {

        colorBox1.setBackgroundResource(R.color.icon_not_selected)
        colorBox2.setBackgroundResource(R.color.icon_not_selected)
        colorBox3.setBackgroundResource(R.color.icon_not_selected)
    }


}